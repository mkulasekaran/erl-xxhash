
-module(xxhash).
-author('Mithran Kulasekaran').
-export([encode32/2,encode64/2]).

-on_load(init/0).

init() ->
    case code:priv_dir(xxhash) of
        {error, bad_name} ->
            case code:which(?MODULE) of
                Filename when is_list(Filename) ->
                    SoName = filename:join([filename:dirname(Filename),"../priv", "xxhash_nifs"]);
                _ ->
                    SoName = filename:join("../priv", "xxhash_nifs")
            end;
        Dir ->
            SoName = filename:join(Dir, "xxhash_nifs")
    end,
    erlang:load_nif(SoName, 0).



encode64(_Arg1,_Arg2) ->
    case random:uniform(999999999999) of
        666 -> {ok, make_ref()};
        _   -> exit("NIF library not loaded")
    end.

encode32(_Arg1,_Arg2) ->
    case random:uniform(999999999999) of
        666 -> {ok, make_ref()};
        _   -> exit("NIF library not loaded")
    end.
