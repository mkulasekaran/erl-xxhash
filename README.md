xxhash library for generating hash in erlang

xxhash:encode"32/64" ( "key to encode (hash)" , seed)

CODE:

```
mithran@mithran-ubuntu:~/xxhash$ erl -pa ebin/
Erlang/OTP 17 [erts-6.2] [source] [64-bit] [smp:8:8] [async-threads:10] [hipe] [kernel-poll:false]

Eshell V6.2  (abort with ^G)
1> xxhash:encode32(<<"test">>,100).
<<"2613218145">>
2> xxhash:encode64(<<"test">>,100).
<<"7635413161476427970">>
```