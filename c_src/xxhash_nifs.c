/*
 * dablooms_nifs.c
 *
 *  Created on: Aug 10, 2014
 *      Author: mithu
 */

#include "xxhash_nifs.h"
#include "erl_nif_compat.h"
#include "xxhash.h"
#include <string.h>
#include <stdio.h>
#define MAXBUFLEN 64

static ERL_NIF_TERM xxhash_encode32(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]);
static ERL_NIF_TERM xxhash_encode64(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]);

static ErlNifFunc nif_funcs[] = { { "encode64", 2, xxhash_encode64 },
		{ "encode32", 2, xxhash_encode32 } };

ERL_NIF_INIT(xxhash, nif_funcs, NULL, NULL, NULL, NULL);

static ERL_NIF_TERM xxhash_encode64(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]) {

	ErlNifBinary data;
	long seed;

	if (enif_inspect_binary(env, argv[0], &data)
			&& enif_get_long(env, argv[1], &seed)) {

		char str[MAXBUFLEN];
		sprintf(str, "%llu", XXH64(data.data, data.size, seed));
		ErlNifBinary output;
		if (!enif_alloc_binary_compat(env, strlen(str), &output)) {
			return enif_make_atom(env,"not_enough_memory");
		}
		memmove(output.data, str, strlen(str));
		return enif_make_binary(env, &output);
	} else {
		return enif_make_badarg(env);
	}
}
static ERL_NIF_TERM xxhash_encode32(ErlNifEnv* env, int argc,
		const ERL_NIF_TERM argv[]) {

	ErlNifBinary data;
	long seed;

	if (enif_inspect_binary(env, argv[0], &data)
			&& enif_get_long(env, argv[1], &seed)) {

		char str[MAXBUFLEN];
		sprintf(str, "%u", XXH32(data.data, data.size, seed));
		ErlNifBinary output;
		if (!enif_alloc_binary_compat(env, strlen(str), &output)) {
			return enif_make_atom(env,"not_enough_memory");
		}
		memmove(output.data, str, strlen(str));
		return enif_make_binary(env, &output);
	} else {
		return enif_make_badarg(env);
	}
}
